from sys import argv
import re

def add_user():
    update_available_uids('add')

def del_user():
    file_ptr = open('del_user.txt')
    deleted_uid = file_ptr.read()
    uid = int(re.search(r'(?<=\=)(.*?)(?=\()',deleted_uid).group(1))
    update_available_uids(uid=uid, operation='delete')

def update_available_uids(operation, uid=None):
    file_ptr = open('availableIds.txt', 'r')
    test = file_ptr.read()
    uid_list = eval(test)
    file_ptr.close()

    if operation is 'delete':
        uid_list.append(uid)
        uid_list.sort()
    elif operation is 'add':
        add_id = uid_list.pop(0)
        with open("roles/create-user/vars/main.yml", "a+") as file_object:
            file_object.seek(0)
            data = file_object.read(80)
            if len(data) > 0 :
                file_object.write(' uid: "'+str(add_id)+'"')
        file_object.close()
    
    file_ptr = open('availableIds.txt', 'w')
    file_ptr.write(str(uid_list))
    file_ptr.close()


operation = argv[1]
if operation == "add":
    add_user()
elif operation == "delete":
    del_user()
